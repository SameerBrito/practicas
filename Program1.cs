using System;

namespace Practica_3._1
{
    class Program
    {
        static void Main(string[] args)
        {
            int x = 1, suma = 0, valor = 1;

            while (valor >= 1)
            {
                Console.Write("Ingresa un valor Positivo: ");
                valor = int.Parse(Console.ReadLine());

                suma = suma + valor;

                x += 1;
            }
            Console.WriteLine("La suma de los numeros es: ");
            Console.WriteLine(suma);
        }
    }
}
