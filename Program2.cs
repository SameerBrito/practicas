using System;

namespace Practica_3._2
{
    class Program
    {
        static void Main(string[] args)
        {
            int numero = 0;
            do
            {
                if (numero >= 0)
                {
                    numero = numero + 1;

                    Console.WriteLine(numero);
                }

            } while (numero != 10);
            Console.ReadKey();
        }
    }
}
