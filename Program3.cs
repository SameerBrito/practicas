using System;

namespace Practica_3._3
{
    class Program
    {
        static void Main(string[] args)
        {
            int x, mul1 = 10, mul2 = 10;

            x = mul1 * mul2;

            while (x <= 120)
            {
                Console.WriteLine($"{x}");

                x += 2;
            }
            Console.ReadKey();
        }
    }
}
